# Be sure to restart your web server when you modify this file.
require 'digest/sha1'

# Uncomment below to force Rails into production mode when 
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.1.0' unless defined? RAILS_GEM_VERSION

# customizable app name, used e.g. in windows' title and for making session data unique
APP_NAME = 'Doxroom'

# customizable string with optional markup shown in login page, can be used to suggest guest access credentials
WELCOME_MESSAGE = 'Welcome to Doxroom!'

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
# Personalize the secret phrase here!
  config.action_controller.session = { :session_key => "_#{APP_NAME}_session", :secret => "some secret phrase of at least 30 chars; #{Digest::SHA1.hexdigest(APP_NAME)}" }

end

# Add new inflection rules using the following format 
# (all these examples are active by default):
# Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf
# Mime::Type.register "application/x-mobile", :mobile

# Include your application configuration below

# ActionMailer config:
ActionMailer::Base.delivery_method = :test

ActionMailer::Base.smtp_settings = {
  :address => 'smtp.xs4all.nl',
  :port => 25,
  :domain => 'xs4all.nl'
}

PASSWD_MAILER_ADDR="#{APP_NAME}-mailer@somewhere.invalid"

COPYRIGHT_NOTICE = 'Doxroom derives from <a 
href="http://boxroom.rubyforge.org">Boxroom</a>, &#0169; Mischa Berger 2005-2007'
# Previous dynamic copyright notice tracked usage of the app, I guess content creation must be tracked instead. -- Marcello

WEBSERVER_ADDR = 'http://localhost:3000' # Remember to replace this with the public URI of the server that hosts your boxroom app when you go into production.

# Path where the files will be stored; must exist
UPLOAD_PATH = "#{RAILS_ROOT}/uploads"

# Path where deleted files will be stored; must exist. Set to nil to permanently remove the files when a delete is issued from doxroom
TRASH_PATH = "#{RAILS_ROOT}/public/trash"

# Use upload progress (or not)
# CURRENTLY BROKEN, put false
USE_UPLOAD_PROGRESS = false

# We need acts_as_ferret
require 'acts_as_ferret'

#adding to Errors class a naive method to avoid translation strings for all combinations of model/error. Probably doesn't make sense when syntax differs too much from the original language.
class ActiveRecord::Errors
  def full_messages_globalized
    full_messages = []
    @errors.each_key do |attr|
      @errors[attr].each do |msg|
      next if msg.nil?
        if attr == "base"
          full_messages << msg.t
        else
          full_messages << @base.class.human_attribute_name(attr).t + " " + msg.first.t
        end
      end
    end
    full_messages
  end
end

begin
  include Globalize
  Locale.set_base_language('en-US')
  Locale.set('en-US')  #Set the default language
  #Locale.set('it-IT') to enable italian localization
rescue #don't stop for exceptions that migrations may throw initially
end

DEFAULT_STYLESHEET = 'doxroom' #original is 'boxroom'

# Define the helpers that extract the plain-text to be indexed
# Disable those unavailable to your system
# pdftotext AFAIK is part of linux package xpdf
INDEX_HELPERS = [ # defines helpers
  # Examples:
  { :ext => /rtf$/, :helper => 'unrtf --text %s', :remove_before => /-----------------/ },
  { :ext => /pdf$/, :helper => 'pdftotext -layout -enc UTF-8 %s -', :file_output => false },
  { :ext => /doc$/, :helper => 'antiword %s', :remove_before => /-----------------/ }
]
# For Pros: One could add a helper for html if no charset issues arise with { :ext => /(s|x)?htm(l|ls)?$/, :helper => 'html2text -nobs %s | iconv -c -f ISO-8859-1 -t UTF-8', :file_output => false }
# If this is the case, note that a primitive html to text conversion is done in myfile.rb, and ought to be disabled.

# Map of filename extensions to MIME type that a browser can display. These will enable the "preview in browser" button for such extensions.
INDEX_MIMETYPES = {
  ".jpg"=>"image/jpeg",
  ".jpeg"=>"image/jpeg",
  ".gif"=>"image/gif",
  ".png"=>"image/png",
  ".svg"=>"image/svg",
  ".txt"=>"text/plain",
  ".text"=>"text/plain",
  ".htm"=>"text/html",
  ".htmls"=>"text/html",
  ".html"=>"text/html",
  ".xml"=>"text/xml"}


# thumbnail generation helper
HELPER_THUMBNAILS='mini_magick/mini_magick'  # mini magick is included in doxroom. If problems arise another possible option is 'RMagick'

HELPER_THUMBNAILS = nil unless %x[identify -version].index('ImageMagick') # resets if imagemagick not installed. nil is fallback setting: no thumbnails


require HELPER_THUMBNAILS if HELPER_THUMBNAILS

# file extensions for which thumbnails will be created
# .pdf currently doesn't work with mini_magick and is slow with RMagick
INDEX_THUMB_EXT=['.jpg','.jpeg','.gif','.tif','.tiff','.png']

# attributes of a generated thumbnail, currently width is considered
THUMB_ATTRS={:width => 100, :height => 100}

# whitelist that allows local files with matching paths to be linked
# see "link" and "do_the_link" actions in file controller
# An example for multiple paths:
# LINK_WHITELIST = Regexp.new('(^/home/user/|^/mnt/smbshare/)')
LINK_WHITELIST = Regexp.new('^/home/')

FORCE_SSL=false #this needs a properly configured webserver. Note that login and password travel in clear if this is disabled.

TagList.delimiter = "," # the character that will be used to separate tags

if TagList.delimiter == " "
  TAG_SEPARATOR=TagList.delimiter
else
  TAG_SEPARATOR=TagList.delimiter + " " # to improve readability, rendering 
end

MAX_UPLOAD_FILE_SIZE = 10*1024*1024 # in bytes, limits the size of uploaded files, ".part" is added to filenames when exceeding the limit and partial upload is retained.
# Set to nil to disable.
# Note: accuracy of limit depends on transfer size, better set it to a multiple of 4096, see myfile.rb


# Mailbox configuration: a POP mailbox can be checked for messages and 
# attachments to be inserted as documents. I suggest a custom mailbox or 
# filtering the mailbox to delete spam before downloading.
POP_SSL = true # if SSL is used by POP
POP_ADDR = 'pop.gmail.com' # Address of the POP server
POP_PORT = 995 # Default is 110, 995 is used for SSL
POP_USERNAME = 'username@gmail.com.invalid' # add .invalid to disable mailbox
POP_PASSWORD = '12345' # the combination on my luggage ;)

# Antivirus configuration:
# Set AV_CMD to command and options needed to scan an incoming file
# %File is the placeholder for the incoming file. examples:
# AV_CMD=nil disables scanning altogether.
# AV_CMD='clamdscan %File' scans with clamav daemon installed
# AV_CMD='clamscan %File' scans with clamav without daemon, likely much less responsive than clamdscan
AV_CMD=nil 
AV_WARNING='VIRUS/AV_SCAN_FAILED!' # if scan returns failure, file gets tagged with this.

STATIC_DOWNLOAD= :http
# false -> download through the app (inefficient but secure) - no need to set DOWNLOAD_DOCUMENT_ROOT
# :http -> download through the server (fast but it's easy to hotlink) - DOWNLOAD_DOCUMENT_ROOT must be a subfolder of public/
# :http_rand -> download from randomized links through the webserver (fast, secure enough, pollutes fs with hard links) - DOWNLOAD_DOCUMENT_ROOT must be a subfolder of public/ and it's necessary to clean it up to prevent hotlinking and cruft. Done by a script that can invoked by cron (e.g. "5-55/6 * * * * /[full rails application path]/script/cleanup_dl_links"; make sure it's executable and has write permission to the downloads folder) 
# :lighty_modsecdownload -> use lighttpd and mod_secdownload (fast, efficient and difficult to abuse). In this case the following settings become relevant and should closely match the corresponding entries in lighttpd configuration, together with DOWNLOAD_DOCUMENT_ROOT, which can use your app uploads folder.

DOWNLOAD_DOCUMENT_ROOT="#{RAILS_ROOT}/public/downloads/" 
MODSECDOWNLOAD_URI_PREFIX='/dl/'
MODSECDOWNLOAD_SECRET='secret' # can be a random alphanumeric sequence

GUEST_USERS=['demo','guest'] # a guest user won't be able to alter its attributes, so a guest can't take over the account by changing password.

FOLDER_LIST_LIMIT=30 #how many items to show in a listing by default.
HISTORY_LIST_LIMIT=50 #same for activity
RESULT_LIST_LIMIT=30  # and results, this one can be :all

