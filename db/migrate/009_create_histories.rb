class CreateHistories < ActiveRecord::Migration
  def self.up
    create_table :histories do |t|
      t.references :user
      t.references :folder #the idea is to track the changes happening into a folder by logging each item's parent
      t.text :description
      t.text :data
      t.string :remote_ip
      t.references :recallable, :polymorphic => true # this references the item we track.

      t.timestamps
    end
    add_index :histories, :user_id
    add_index :histories, :updated_at
    add_index :histories, :remote_ip
    add_index :histories, :folder_id
  end

  def self.down
    remove_index :histories, :user_id
    remove_index :histories, :updated_at
    remove_index :histories, :remote_ip
    remove_index :histories, :folder_id
    drop_table :histories
  end
end
