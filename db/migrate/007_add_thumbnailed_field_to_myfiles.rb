class AddThumbnailedFieldToMyfiles < ActiveRecord::Migration
  def self.up
    # Add indexed column to Myfiles, used to see which files were indexed by Ferret (SH)
    add_column :myfiles, 'thumbnailed', :boolean, :default => false
  end

  def self.down
    remove_column :myfiles, :thumbnailed
  end
end