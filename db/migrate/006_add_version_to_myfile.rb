class AddVersionToMyfile < ActiveRecord::Migration
  def self.up
    # Add version column to myfiles table.
    add_column :myfiles, 'version', :integer, :default => 0
    add_index :myfiles, :version
  end

  def self.down
    remove_index :myfiles, :version
    remove_column :myfiles, 'version'
  end
end