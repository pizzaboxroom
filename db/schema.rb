# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 9) do

  create_table "folders", :force => true do |t|
    t.string   "name"
    t.datetime "date_modified"
    t.integer  "user_id",       :default => 0
    t.integer  "parent_id",     :default => 0
    t.boolean  "is_root",       :default => false
  end

  add_index "folders", ["is_root"], :name => "index_folders_on_is_root"
  add_index "folders", ["parent_id"], :name => "index_folders_on_parent_id"
  add_index "folders", ["user_id"], :name => "index_folders_on_user_id"
  add_index "folders", ["date_modified"], :name => "index_folders_on_date_modified"
  add_index "folders", ["name"], :name => "index_folders_on_name"

  create_table "globalize_countries", :force => true do |t|
    t.string "code",                   :limit => 2
    t.string "english_name"
    t.string "date_format"
    t.string "currency_format"
    t.string "currency_code",          :limit => 3
    t.string "thousands_sep",          :limit => 2
    t.string "decimal_sep",            :limit => 2
    t.string "currency_decimal_sep",   :limit => 2
    t.string "number_grouping_scheme"
  end

  add_index "globalize_countries", ["code"], :name => "index_globalize_countries_on_code"

  create_table "globalize_languages", :force => true do |t|
    t.string  "iso_639_1",             :limit => 2
    t.string  "iso_639_2",             :limit => 3
    t.string  "iso_639_3",             :limit => 3
    t.string  "rfc_3066"
    t.string  "english_name"
    t.string  "english_name_locale"
    t.string  "english_name_modifier"
    t.string  "native_name"
    t.string  "native_name_locale"
    t.string  "native_name_modifier"
    t.boolean "macro_language"
    t.string  "direction"
    t.string  "pluralization"
    t.string  "scope",                 :limit => 1
  end

  add_index "globalize_languages", ["rfc_3066"], :name => "index_globalize_languages_on_rfc_3066"
  add_index "globalize_languages", ["iso_639_3"], :name => "index_globalize_languages_on_iso_639_3"
  add_index "globalize_languages", ["iso_639_2"], :name => "index_globalize_languages_on_iso_639_2"
  add_index "globalize_languages", ["iso_639_1"], :name => "index_globalize_languages_on_iso_639_1"

  create_table "globalize_translations", :force => true do |t|
    t.string  "type"
    t.string  "tr_key"
    t.string  "table_name"
    t.integer "item_id"
    t.string  "facet"
    t.boolean "built_in",            :default => true
    t.integer "language_id"
    t.integer "pluralization_index"
    t.text    "text"
    t.string  "namespace"
  end

  add_index "globalize_translations", ["table_name", "item_id", "language_id"], :name => "globalize_translations_table_name_and_item_and_language"
  add_index "globalize_translations", ["tr_key", "language_id"], :name => "index_globalize_translations_on_tr_key_and_language_id"

  create_table "group_permissions", :force => true do |t|
    t.integer "folder_id",  :default => 0
    t.integer "group_id",   :default => 0
    t.boolean "can_create", :default => false
    t.boolean "can_read",   :default => false
    t.boolean "can_update", :default => false
    t.boolean "can_delete", :default => false
  end

  add_index "group_permissions", ["can_delete"], :name => "index_group_permissions_on_can_delete"
  add_index "group_permissions", ["can_update"], :name => "index_group_permissions_on_can_update"
  add_index "group_permissions", ["can_read"], :name => "index_group_permissions_on_can_read"
  add_index "group_permissions", ["can_create"], :name => "index_group_permissions_on_can_create"
  add_index "group_permissions", ["group_id"], :name => "index_group_permissions_on_group_id"
  add_index "group_permissions", ["folder_id"], :name => "index_group_permissions_on_folder_id"

  create_table "groups", :force => true do |t|
    t.string  "name"
    t.boolean "is_the_administrators_group", :default => false
  end

  add_index "groups", ["is_the_administrators_group"], :name => "index_groups_on_is_the_administrators_group"
  add_index "groups", ["name"], :name => "index_groups_on_name"

  create_table "groups_users", :id => false, :force => true do |t|
    t.integer "group_id", :default => 0
    t.integer "user_id",  :default => 0
  end

  add_index "groups_users", ["group_id", "user_id"], :name => "index_groups_users_on_group_id_and_user_id"

  create_table "histories", :force => true do |t|
    t.integer  "user_id"
    t.integer  "folder_id"
    t.text     "description"
    t.text     "data"
    t.string   "remote_ip"
    t.integer  "recallable_id"
    t.string   "recallable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "histories", ["folder_id"], :name => "index_histories_on_folder_id"
  add_index "histories", ["remote_ip"], :name => "index_histories_on_remote_ip"
  add_index "histories", ["updated_at"], :name => "index_histories_on_updated_at"
  add_index "histories", ["user_id"], :name => "index_histories_on_user_id"

  create_table "myfiles", :force => true do |t|
    t.string   "filename"
    t.integer  "filesize"
    t.datetime "date_modified"
    t.integer  "folder_id",     :default => 0
    t.integer  "user_id",       :default => 0
    t.boolean  "indexed",       :default => false
    t.integer  "version",       :default => 0
    t.boolean  "thumbnailed",   :default => false
  end

  add_index "myfiles", ["version"], :name => "index_myfiles_on_version"
  add_index "myfiles", ["indexed"], :name => "index_myfiles_on_indexed"
  add_index "myfiles", ["user_id"], :name => "index_myfiles_on_user_id"
  add_index "myfiles", ["folder_id"], :name => "index_myfiles_on_folder_id"
  add_index "myfiles", ["date_modified"], :name => "index_myfiles_on_date_modified"
  add_index "myfiles", ["filesize"], :name => "index_myfiles_on_filesize"
  add_index "myfiles", ["filename"], :name => "index_myfiles_on_filename"

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at"
  end

  add_index "taggings", ["taggable_id", "taggable_type"], :name => "index_taggings_on_taggable_id_and_taggable_type"
  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "usages", :force => true do |t|
    t.datetime "download_date_time"
    t.integer  "myfile_id",          :default => 0
    t.integer  "user_id",            :default => 0
  end

  add_index "usages", ["user_id"], :name => "index_usages_on_user_id"
  add_index "usages", ["myfile_id"], :name => "index_usages_on_myfile_id"
  add_index "usages", ["download_date_time"], :name => "index_usages_on_download_date_time"

  create_table "users", :force => true do |t|
    t.string  "name"
    t.string  "email"
    t.string  "hashed_password"
    t.boolean "is_the_administrator", :default => false
    t.string  "password_salt"
    t.string  "rss_access_key"
  end

  add_index "users", ["rss_access_key"], :name => "index_users_on_rss_access_key"
  add_index "users", ["password_salt"], :name => "index_users_on_password_salt"
  add_index "users", ["is_the_administrator"], :name => "index_users_on_is_the_administrator"
  add_index "users", ["hashed_password"], :name => "index_users_on_hashed_password"
  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["name"], :name => "index_users_on_name"

end
