namespace :ferret do
    desc "rebuild ferret indexes"
    task :rebuild   => :environment do
      [Folder, Myfile].each{|klass|
        puts "rebuilding the Ferret /#{RAILS_ENV}/ index for : 
#{klass}.."
        klass.rebuild_index
        puts "  done (Warning: each Myfile instance has to be 
reindexed too, see reindex_myfiles).
"
        }
    end
end

namespace :ferret do
    desc "reindex Myfile instances"
    task :reindex_myfiles   => :environment do
      (Myfile.find :all).each do |f|
        puts "rebuilding the Ferret /#{RAILS_ENV}/ index for : 
#{f.filename}.."
        f.reindex
        puts "  done.
"
      end
    end
end

namespace :db do
  desc "remove old activity (run w niceness)"
  task :clean_history => :environment do
    History.cleanup
  end
  puts "  done.
"
end
