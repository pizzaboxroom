require "cgi.rb"

class CGI
# from ruby 1.8.4 cgi.rb
# overloads a method to add some useful entity conversion for italian users

  def CGI::unescapeHTML(string)
    string.gsub(/&(.*?);/n) do
      match = $1.dup
      case match
      when /\Aamp\z/ni           then '&'
      when /\Aquot\z/ni          then '"'
      when /\Agt\z/ni            then '>'
      when /\Alt\z/ni            then '<'
      when /\Aegrave\z/ni then 'è'
      when /\Aagrave\z/ni then 'à'
      when /\Aograve\z/ni then 'ò'
      when /\Aigrave\z/ni then 'ì'
      when /\Augrave\z/ni then 'ù'
      when /\Aucirc\z/ni  then 'û'
      when /\Aeacute\z/ni then 'é'
      when /\A#0*(\d+)\z/n       then
        if Integer($1) < 256
          Integer($1).chr
        else
          if Integer($1) < 65536 and ($KCODE[0] == ?u or $KCODE[0] == ?U)
            [Integer($1)].pack("U")
          else
            "&##{$1};"
          end
        end
      when /\A#x([0-9a-f]+)\z/ni then
        if $1.hex < 256
          $1.hex.chr
        else
          if $1.hex < 65536 and ($KCODE[0] == ?u or $KCODE[0] == ?U)
            [$1.hex].pack("U")
          else
            "&#x#{$1};"
          end
        end
      else
        "&#{match};"
      end
    end
  end
end