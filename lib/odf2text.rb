# Code adapted from a mark watson's blog entry
# http://markwatson.com/blog/2007/05/why-odf-is-better-than-microsofts.html
# Used with permission

#require 'rubygems'
require 'rexml/document'
require 'rexml/streamlistener'
include REXML

class ODFHandler
  include StreamListener 
  attr_reader :plain_text
  def initialize; @plain_text = ""; @last_tag_name =""; end     
  def tag_start name, attrs; @last_tag_name = name; end
  def text s
    @plain_text << s << "\n" if @last_tag_name.index('text')
  end
end

class ReadOpenOffice
  attr_reader :text
  def initialize file_path
    Zip::ZipFile.open(file_path) { |zipFile|
      xml_handler = ODFHandler.new
      Document.parse_stream((zipFile.read('content.xml')), xml_handler)
      @text = xml_handler.plain_text
    }
  end
end