

# Adds convenient method to taglist
class TagList < Array
  def remove_wildcard(*names)
    extract_and_apply_options!(names)
    names.each do |n|
      delete_if {|name| name.index(n.chomp('*')) == 0 } if n[-1] == 42 # 42 is character code for '*'
    end
    self
  end
end