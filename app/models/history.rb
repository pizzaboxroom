class History < ActiveRecord::Base
  belongs_to :user
  belongs_to :folder # to track changes from the perspective of the containing folder
  belongs_to :recallable, :polymorphic => true #link to the item
  
  serialize :data, Hash
  
  # remove old data - warning: memory consuming unless run often
  def self.cleanup(months=2,keep_last=10)
    raise IndexError if keep_last < 1 
    (History.find(:all, :conditions => ["updated_at < ?",(Date.today << months)])).each do |item|
      f=item.recallable
      if f
        h=f.histories.find(:all, :order => 'created_at ASC')
        next if item==h.first # keep the creation event
        next if (h[-keep_last..-1] || []).index item
        item.destroy
      else # history item does not refer to an existing object
        item.destroy
      end
    end
  end
end
