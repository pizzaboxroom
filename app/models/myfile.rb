require 'zip/zipfilesystem'
require 'odf2text' # in 'lib' directory
require 'unescapeHTML'
require 'md5'
require 'fileutils'

# Files in the database are represented by Myfile.
# It's called Myfile, because File is a reserved word.
# Files are in (belong to) a folder and are uploaded by (belong to) a User.
class Myfile < ActiveRecord::Base
  acts_as_ferret({:remote => true, :store_class_name => true, :fields => { :text => { :store => :yes }, :filename => { :store => :no }, :tag_list => { :store => :no }, :user_id => { :store => :no }, :folder_id => { :store => :no }, :version => {:store => :no}}},{:analyzer => Ferret::Analysis::StandardAnalyzer.new(stop_words = nil, lower=true), :max_field_length => Ferret::FIX_INT_MAX})

  acts_as_taggable
  
  belongs_to :folder
  belongs_to :user
  has_many :usages, :dependent => :destroy
  has_many :histories, :as => :recallable

  validates_uniqueness_of :filename, :scope => [:folder_id, :version]
  validates_presence_of :filename

  before_destroy :delete_file_on_disk
  after_destroy :log_deletion
  after_create :rename_newfile, :add_thumbnail

  # Accessor that receives the data from the form in the endview.
  # The file will be saved in a folder called 'uploads'.
  # (See: AWDWR pp. 362.)
  def myfile=(myfile_field)
    if myfile_field and myfile_field.length > 0

      filename=save_to_fs(myfile_field)

      begin
        index_content(filename)
      rescue Exception
      end

      # Save it all to the model
      self.filename = filename
      self.filesize = (File.size(self.temp_path) / 1024.0).ceil
    end
  end
  
  def expand(myfile_field)
    return unless myfile_field and myfile_field.length > 0
    filename=save_to_fs(myfile_field)
    return if filename[-5..-1]=='.part' #do not attempt if incomplete
    expand_path=self.temp_path.concat('-dir')
    FileUtils.mkdir(expand_path)
    return unless system('unzip ' + self.temp_path + ' -d ' + expand_path) # this is OS specific but efficient
    return expand_path
  end

  # Instead of getting a file from the client, link to a local file
  # NOTE: platform dependent!
  def hardlink_to(path)
    filename = File.basename(path)
    @date_time_created = Time.now.to_f
    File.link path, self.temp_path
    
    begin
      index_content(filename)
    rescue Exception
    end
    
    self.filename = filename
    self.filesize = (File.size(path) / 1024.0).ceil
  end
  
  #create a new doc from an email
  def from_email(email)
    filename=email.subject + '(' + email.date.strftime('%Y%m%d[%H:%M:%S]') + ').txt'
    @date_time_created = Time.now.to_f
    
    #save file
    FileUtils.mkpath(File.dirname(self.temp_path)) #create subdir if needed
    File.open(self.temp_path,'wb') do |f|
      f.write(email.body)
    end
    
    self.indexed = true if self.text = email.body
    
    #save into DB
    self.filename = filename
    self.filesize = (email.body.length / 1024.0).ceil
    
  end
  
  attr_writer :text # Setter for text

  # Getter for text.
  # If text is blank get the text from the index.
  def text
    @text = Myfile.aaf_index.ferret_index[self.document_number][:text] if @text.blank?
  end
  
  # The file in the uploads folder has the same name as the id of the file.
  # This must be done after_create, because the id won't be available any earlier.
  def rename_newfile
    definitive=self.path
    definitive+='.virus' if self.tag_list.include?(AV_WARNING) # this makes marked file inaccessible, a good things for suspects
    FileUtils.mkpath(File.dirname(self.path)) #make containing dir
    File.rename self.temp_path, definitive
  end
  
  # makes a hard link so a file with proper name and extension is downloaded directly from the webserver.
  def mklink
    rel_dir=self.rel_dir
    abs_dir=DOWNLOAD_DOCUMENT_ROOT.chomp('/') + rel_dir
    File.makedirs(abs_dir) # create if absent
    linkname=abs_dir+self.filename # needs cleanup scripts, see docs
    File.link(self.path,linkname) unless File.exist?(linkname)
    return rel_dir+self.filename
  end
  
  
  # create a thumbnail file with the configured helper
  def add_thumbnail
    return if self.tag_list.include?(AV_WARNING) 
    return unless INDEX_THUMB_EXT.include?(File.extname(self.filename).downcase) 
    FileUtils.mkpath(File.dirname(self.thumb_path)) # create the containing subdir
    if HELPER_THUMBNAILS == 'RMagick' && Magick 
      thumb=Magick::Image.read(self.path).first
      thumb.thumbnail!(THUMB_ATTRS[:width],(THUMB_ATTRS[:width]*thumb.rows) / thumb.columns)
      self.update_attributes(:thumbnailed => true) if thumb.write(self.thumb_path)
    end
    if MiniMagick
      thumb=MiniMagick::Image.from_file(self.path)
      thumb.thumbnail(THUMB_ATTRS[:width].to_s) unless thumb[:width].to_i <= THUMB_ATTRS[:width].to_i
      self.update_attributes(:thumbnailed => true) if thumb.write(self.thumb_path)
      thumb.tempfile.delete
    end
    File.chmod(0644,self.thumb_path) if File.exist?(self.thumb_path)
  end

  # Scans file for viruses
  # Gets called just before indexing content
  def av_scan
    return unless AV_CMD
    self.tag_list.add(AV_WARNING) unless system(AV_CMD.gsub('%File', self.temp_path))
  end


  # Delete a file (sets version 0 to -1, which makes the file available in trash. If already in trash  or a not current version, deletes it - removing it or putting into the folder configured in doxroom prefs). 
  def destroy_or_trash
    case self.version
      when -1
        self.destroy
      when 0
        self.version = -1 
        self.save
      else #it's an old version
        self.filename = self.filename + '-' + self.version.to_s
        self.version = -1
        self.save
    end
  end
  
  # When removing a myfile record from the database,
  # the actual files on disk have to be removed too.
  # That is exactly what this method does.
  def delete_file_on_disk
    [self.path, self.path + '.virus', self.thumb_path].each do |path|
      if File.exist?(path)
        File.delete(path) unless TRASH_PATH
        File.move(path,sprintf("%s/%s-%s",TRASH_PATH,self.filename,Myfile.base_part_of(path))) if TRASH_PATH
      end
    end
    if STATIC_DOWNLOAD == :lighty_modsecdownload
      abs_dir = DOWNLOAD_DOCUMENT_ROOT.chomp('/') + self.rel_dir
      FileUtils.rm_rf(abs_dir) if File.exist?(abs_dir)
    end
  end

  # Strip off the path and replace all the non alphanumeric,
  # underscores and periods in the filename with an underscore.
  def self.base_part_of(file_name)
    # NOTE: File.basename doesn't work right with Windows paths on Unix
    # INCORRECT: just_filename = File.basename(file_name.gsub('\\\\', '/')) 
    # get only the filename, not the whole path
    name = file_name.gsub(/^.*(\\|\/)/, '')

    # finally, replace all non alphanumeric, underscore or periods with underscore
    name.gsub(/[^\w\.\-]/, '_') 
  end

  # Returns the location of the temp file
  def temp_path
    "#{UPLOAD_PATH}/#{@date_time_created}"
  end

  # The path of the file
  def path
    "#{UPLOAD_PATH}/#{self.split_id}"
  end
  
  # Path of thumbnail if one was generated
  def thumb_path
    "#{RAILS_ROOT}/public/images/boxthumbs/#{self.split_id}.jpg" #the file extension determines the format
  end
  
  # URL of thumbnail as seen by webserver
  def thumb_url
    "/images/boxthumbs/#{self.split_id}.jpg"
  end
  
  # helper for subpaths with l entries at the most
  def split_id(l=1000)
    i=self.id
    msp=i / l
    lsp=i % l 
    digits=Math.log10(l).to_i
    format="%0#{digits}d/%0#{digits}d"
    sprintf(format,msp,lsp)
  end
  
  # part of a path to the static content
  def rel_dir
    reldir="/#{self.split_id}.alias/"
    if STATIC_DOWNLOAD == :http_rand # randomized and dynamic link
      return reldir + Digest::SHA1.hexdigest(Time.new.to_f.to_s + self.filename) + '/'
    else
      return reldir
    end
  end
  
  # All versions of a file
  def self.versions(myfile)
    find( :all, :conditions => {:folder_id => myfile.folder_id, :filename => myfile.filename}, :order => 'version DESC')
  end
  
  # reindex
  def reindex
    self.text=''
    self.indexed = false
    begin
      index_content(self.filename)
    rescue Exception
    end
    self.save!
  end
  
  #return a download URL based on download type
  def download_url(down_t=STATIC_DOWNLOAD)
    return Myfile.gen_sec_link(self.mklink) if down_t==:lighty_modsecdownload
    if (down_t == :http_rand || down_t == :http)
      static_link = DOWNLOAD_DOCUMENT_ROOT.chomp('/') + self.mklink
      static_link.slice!(/^.*public/)
      return WEBSERVER_ADDR.chomp('/')+static_link
    else
      return nil
    end
  end
  
  # Make a link for lighttpd's modsecdownload
  def self.gen_sec_link(rel_path)
    rel_path.sub!(/^([^\/])/,'/\1') # Make sure it had a leading slash
    timestamp = "%08x" % Time.now.to_i  # Timestamp, to hex
    token = MD5.md5(MODSECDOWNLOAD_SECRET + rel_path + timestamp).to_s    # Token Creation
    '%s%s/%s%s' % [MODSECDOWNLOAD_URI_PREFIX, token, timestamp, rel_path]   # Return the properly formatted string
  end
  
  # for a more complete description
  def extattrs
    attrs=self.attributes
    attrs.merge(:tags => self.tag_list)
  end
  
  private
  # Get file content and add to index using helpers if AV scan succeeds
  def index_content(filename)
    self.av_scan
    return if self.tag_list.include?(AV_WARNING) # do not attempt scanning files with viruses
        # Variable to hold the plain text content of the uploaded file
    text_in_file = nil
    filepath = self.id.blank? ? self.temp_path : self.path
    # remove .part if present, real extension needed
    filename=filename[0..(filename.index(/\.part$/) || filename.length)]
    # Try to get the text from the uploaded file
    case filename
      when /\.txt$/
        text_in_file = File.open(filepath) { |f| f.read }

      when /\.(s|x)?htm(l|ls)?$/ # get the file, strip all <> tags, convert entities
         text_in_file = File.open(filepath) { |f| CGI::unescapeHTML(f.read.gsub(/<head>.*?<\/head>/m,'').gsub(/<.*?>/, ' ')) }

      when /\.sxw$|\.odt$|\.ods$/ # read content.xml from zip file, parse
        text_in_file = ReadOpenOffice.new(filepath).text
    end

    # If it didn't get caught yet, try the helpers
    if text_in_file.blank?
      INDEX_HELPERS.each do |index_helper| # defined in environment.rb
        if filename =~ index_helper[:ext] # a matching helper!   

          if index_helper[:file_output] # a file that writes to an output file
            `#{ sprintf(index_helper[:helper], filepath, filepath + '_copy') }`
            text_in_file = File.open(filepath + '_copy') { |f| f.read }
            File.delete(filepath + '_copy')
          else # we get the contents from stido directly
            text_in_file = `#{ sprintf(index_helper[:helper], filepath) }`
          end

          # Check if we need to remove first part (e.g. unrtf)
          unless index_helper[:remove_before].blank?
            if index_helper[:remove_before].match(text_in_file)
              text_in_file = Regexp::last_match.post_match 
            end
          end

          # Check if we need to remove last part
          unless index_helper[:remove_after].blank?
            if index_helper[:remove_after].match(text_in_file)
              text_in_file = Regexp::last_match.pre_match
            end
          end
        end
      end
    end

    unless text_in_file.blank?
      self.text = text_in_file.strip # assign text_in_file to self.text to get it indexed
      self.indexed = true
    end
  end
  
  def log_deletion
    self.histories.create!(:user_id => nil, :description => 'deleted', :data => self.attributes)
  end

  def save_to_fs(myfile_field)
    # Get the filename
    filename = Myfile.base_part_of(myfile_field.original_filename)

    # Set date_time_created,
    # this will be the files temporary name.
    # (this instance variable is also used in temp_path)
    @date_time_created = Time.now.to_f

    # Save the file on the file system
    read_bytes=0
    
    File.open(self.temp_path, 'wb') do |f|
      while (buff = myfile_field.read(4096)) and (( read_bytes += 4096 ) <= (MAX_UPLOAD_FILE_SIZE || 1e17 )) # 1e17 is a hardwired limit of a few petabytes.
        f.write(buff)
      end
    end
    filename = read_bytes <= (MAX_UPLOAD_FILE_SIZE || 1e17) ? filename : filename.concat('.part')
    return filename
  end
  

end