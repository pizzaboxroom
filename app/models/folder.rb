# A folder is a place where files can be stored.
# Folders can also have sub-folders.
# Via groups it is determined which actions the logged-in User can perform.
class Folder < ActiveRecord::Base

  acts_as_taggable
  acts_as_ferret({:remote => true, :store_class_name => true, :fields => { :name => { :store => :no }, :tag_list => { :store => :no }}},{:analyzer => Ferret::Analysis::StandardAnalyzer.new(stop_words = nil, lower=true), :max_field_length => Ferret::FIX_INT_MAX})
  acts_as_tree :order => 'name'

  belongs_to :user
  has_many :myfiles, :dependent => :nullify
  has_many :group_permissions, :dependent => :destroy
  has_many :histories, :as => :recallable

  validates_uniqueness_of :name, :scope => 'parent_id'
  validates_presence_of :name

  attr_accessible :name, :tag_list
  attr_accessor :found_all_files, :count_all_files

  after_destroy :log_deletion

  # List subfolders
  # for the given user in the given order.
  def list_subfolders(logged_in_user, order)
    folders = []
    if logged_in_user.can_read(self.id)
      self.children.find(:all, :order => order).each do |sub_folder|
        folders << sub_folder if logged_in_user.can_read(sub_folder.id)
      end
    end

    # return the folders:
    return folders
  end

  # List the files
  # for the given user in the given order.
  def list_files(logged_in_user, order, limit = nil, offset = nil)
    files = []
    self.count_all_files = 0
    limit ||= FOLDER_LIST_LIMIT
    if logged_in_user.can_read(self.id)
      files = self.myfiles.find(:all, :conditions => {:version => 0}, :order => order, :limit => limit, :offset => offset || 0)
      self.count_all_files=self.myfiles.count(:all, :conditions => {:version => 0})
    end

    #Get data for pagination purposes
    
    self.found_all_files = true
    self.found_all_files = false if (limit.to_i > 0) &&
    (self.count_all_files > limit.to_i)
    self.found_all_files = false if (offset.to_i > 0) && (!files.empty?)

    # return the files:
    return files
  end

  # Returns whether or not the root folder exists
  def self.root_folder_exists?
    folder = Folder.find_by_is_root(true)
    return (not folder.blank?)
  end

  # Create the Root folder
  def self.create_root_folder
    if User.admin_exists? #and  Folder.root_folder_exists?
      folder = self.new
      folder.name = 'Root folder'.t
      folder.date_modified = Time.now
      folder.is_root = true

      # This folder is created by the admin
      if user = User.find_by_is_the_administrator(true)
        folder.user = user
      end

      folder.save # this hopefully returns true
    end
  end
  
  # move to different folder, permission must be checked
  def move(dest_id)
    self.parent_id=dest_id unless (self.id == dest_id || self.allchildren.include?(dest_id))
    self.save!
  end
  
  # return IDs of all subfolders, ignoring access rules
  def allchildren(l=[])
    self.children.each do |child|
      l << child.id
      l + child.allchildren(l)
    end
    return l
  end
  
  # returns the path from root to the folder,
  def full_path(hide_root=true)
    return self.name if self.is_root
    a=self.ancestors.map{|m| m.name}.reverse
    a.slice!(0) if hide_root
    fp = a.empty? ? '' : a.join('/') + '/'
    return fp + self.name
  end
  
    # for a more complete description
  def extattrs
    attrs=self.attributes
    attrs.merge(:tags => self.tag_list)
  end
  
  #first thrashes files
  def destroy
    self.myfiles.each do |f|
        Myfile.versions(f).each do |v| 
          v.folder_id=1
          v.destroy_or_trash
        end
    end
    super
  end
  
  private
  
  def log_deletion
    self.histories.create!(:user_id => nil, :description => 'deleted', :data => self.attributes)
  end

end