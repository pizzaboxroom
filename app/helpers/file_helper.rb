# Helper methods for file views
require 'md5'

module FileHelper
  # Replace 'myfile' with 'file' in a message
  def myfile_to_file(msg)
    return msg.sub('myfile', 'file') if msg
  end
  
  # Make a link for lighttpd's modsecdownload
  def gen_sec_link(rel_path)
    rel_path.sub!(/^([^\/])/,'/\1') # Make sure it had a leading slash
    timestamp = "%08x" % Time.now.to_i  # Timestamp, to hex
    token = MD5.md5(MODSECDOWNLOAD_SECRET + rel_path + timestamp).to_s    # Token Creation
    '%s%s/%s%s' % [MODSECDOWNLOAD_URI_PREFIX, token, timestamp, rel_path]   # Return the properly formatted string
  end
end