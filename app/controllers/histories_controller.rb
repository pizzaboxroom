class HistoriesController < ApplicationController
  # GET /histories
  # GET /histories.xml
  
  before_filter :authorize_admin
  
  def index
    params[:limit] ||= HISTORY_LIST_LIMIT
    params[:offset] ||= 0
    @histories = History.find(:all, :order => (params[:order] || 'created_at DESC'), :limit => params[:limit], :offset => params[:offset], :include => [:user, :folder]).reverse

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @histories }
    end
  end

  # GET /histories/1
  # GET /histories/1.xml
  def show
    @history = History.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @history }
    end
  end

  # DELETE /histories/1
  # DELETE /histories/1.xml
  def destroy
    @history = History.find(params[:id])
    @history.destroy

    respond_to do |format|
      format.html { redirect_to(histories_url) }
      format.xml  { head :ok }
    end
  end
  

end
