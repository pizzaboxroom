# Application-wide functionality used by controllers.
class ApplicationController < ActionController::Base
# Pick a unique cookie name to distinguish our session data from others'
#  session :session_key => "_#{APP_NAME}_session_id"
  before_filter :redirect_to_ssl # use https
  before_filter :authorize # user should be logged in

  # Returns the id of the current folder, which is used by the
  # CRUD authorize methods to check the logged in user's permissions.
  def folder_id
    case params[:controller] + '/' + params[:action]
    when 'folder/index', 'folder/list', 'folder/new', 'folder/create', 'folder/update_permissions', 'folder/feed', 'file/upload', 'file/link', 'file/validate_filename'
      current_folder_id = 1 unless current_folder_id = params[:id]
    when 'file/do_the_upload', 'file/do_the_link', 'file/inbox'
      # This prevents a URL like 0.0.0.0/file/do_the_upload/12,
      # which breaks the upload progress. The URL now looks like this:
      # 0.0.0.0/file/do_the_upload/?folder_id=12
      current_folder_id = 1 unless current_folder_id = params[:folder_id]
    when 'folder/rename', 'folder/update', 'folder/destroy', 'folder/move'
      current_folder_id = @folder.parent_id if @folder
    when 'file/download','file/open_inline', 'file/rename', 'file/update', 'file/destroy', 'file/preview', 'file/update_preview', 'file/reindex'
      current_folder_id = @myfile.folder.id
    when 'folder/list_versions'
      @myfile=Myfile.find(params[:myfile_id])
      current_folder_id = @myfile.folder.id if @myfile
    end
    return current_folder_id
  end

  # Check if a folder exists before executing an action.
  # If it doesn't exist: redirect to 'list' and show an error message
  def does_folder_exist
    @folder = Folder.find(params[:id]) if params[:id]
  rescue
    flash.now[:folder_error] = 'Someone else deleted the folder you are using. Your action was cancelled and you have been taken back to the root folder.'.t
    redirect_to :controller => 'folder', :action => 'list' and return false
  end

  # The #authorize method is used as a <tt>before_hook</tt> in most controllers.
  # If the session does not contain a valid user, the method redirects to either
  # AuthenticationController.login or AuthenticationController.create_admin (if no users exist yet).
  def authorize
    @logged_in_user = User.find(session[:user_id])
  rescue
    reset_session
    @logged_in_user = nil
    if User.find(:all).length > 0
      session[:jumpto] = request.parameters
      redirect_to :controller => 'authentication', :action => 'login' and return false
    else
      redirect_to :controller => 'authentication', :action => 'create_admin' and return false
    end
  end

  # If the session does not contain a user with admin privilages (is in the admins
  # group), the method redirects to /folder/list
  def authorize_admin
    redirect_to(:controller => 'folder', :action => 'list') and return false unless @logged_in_user.is_admin?
  end

  # Redirect to the Root folder and show an error message
  # if current user cannot create in current folder
  def authorize_creating
    unless @logged_in_user.can_create(folder_id)
      flash.now[:folder_error] = "You don't have create permissions for this folder.".t
      redirect_to :controller => 'folder', :action => 'list', :id => folder_id and return false
    end
  end

  # Redirect to the Root folder and show an error message
  # if current user cannot read in current folder
  def authorize_reading
    unless @logged_in_user.can_read(folder_id)
      flash.now[:folder_error] = "You don't have read permissions for this folder.".t
      redirect_to :controller => 'folder', :action => 'list', :id => nil and return false
    end
  end

  # Redirect to the Root folder and show an error message
  # if current user cannot update in current folder
  def authorize_updating
    unless @logged_in_user.can_update(folder_id)
      flash.now[:folder_error] = "You don't have update permissions for this folder.".t
      redirect_to :controller => 'folder', :action => 'list', :id => folder_id and return false
    end
  end

  # Check if the logged in user has permission to delete the file
  def authorize_deleting
    unless @logged_in_user.can_delete(folder_id)
      flash.now[:folder_error] = "You don't have delete permissions for this folder.".t
      redirect_to :controller => 'folder', :action => 'list', :id => folder_id and return false
    end
  end
  
  #instance must have the proper polymorphic association, like folder and myfile do
  def log_the_event(instance,args)
    instance.histories.create!(:user_id => @logged_in_user.id, :remote_ip => request.remote_ip, :description => args[:description], :data => args[:data], :folder_id => args[:folder_id] || folder_id )
  end
  
  # Copy the GroupPermissions of the parent folder to the given folder
  def copy_permissions_to_new_folder(folder)
    # get the 'parent' GroupPermissions
    GroupPermission.find_all_by_folder_id(folder_id).each do |parent_group_permissions|
      # create the new GroupPermissions
      group_permissions = GroupPermission.new
      group_permissions.folder = folder
      group_permissions.group = parent_group_permissions.group
      group_permissions.can_create = parent_group_permissions.can_create
      group_permissions.can_read = parent_group_permissions.can_read
      group_permissions.can_update = parent_group_permissions.can_update
      group_permissions.can_delete = parent_group_permissions.can_delete
      group_permissions.save
    end
  end
  
    # uses diff to return what's changed, SO dependent
  def text_diff(astring,bstring)
    a="#{UPLOAD_PATH}/#{Time.now.to_f}-diff"
    b=a+'2'
    File.open(a,'wb') do |f|
      f.write astring
      f.write "\n"
    end
    File.open(b,'wb') do |f|
      f.write bstring
      f.write "\n"
    end
    result=%x[diff #{a} #{b}]
    File.rm_f a
    File.rm_f b
    return result
  end

  private
  # force SSL except for local requests, if configured to do so
  def redirect_to_ssl
    # init an instance variable so that a view can show options available to local users
    @local_request = local_request?
    redirect_to :protocol => "https://" and return false unless (request.ssl? || !FORCE_SSL || @local_request)
  end

  #full path of contained files/folders, no recursion
  def ls_one_level(path)
    contents=Dir.entries(path).map{|i| File.join(path,i) unless (i=='.'||i=='..')}
    contents.delete(nil)
    return contents
  end
end