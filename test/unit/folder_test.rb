require File.dirname(__FILE__) + '/../test_helper'

class FolderTest < Test::Unit::TestCase
  fixtures :folders

  def setup
    @folder = Folder.find(1)
  end

  # Replace this with your real tests.
  def test_truth
    assert_kind_of Folder,  @folder
  end
end
