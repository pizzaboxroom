require File.dirname(__FILE__) + '/../test_helper'
require 'password_mailer'

class PasswordMailerTest < Test::Unit::TestCase
  FIXTURES_PATH = File.dirname(__FILE__) + '/../fixtures'
  CHARSET = "utf-8"

  include ActionMailer::Quoting

  def setup
    ActionMailer::Base.delivery_method = :test
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []

    @expected = TMail::Mail.new
    @expected.set_content_type "text", "plain", { "charset" => CHARSET }
  end

  def test_new_user
    @expected.subject = 'PasswordMailer#new_user'
    @expected.body    = read_fixture('new_user')
    @expected.date    = Time.now

    assert_equal @expected.encoded, PasswordMailer.create_new_user(@expected.date).encoded
  end

  def test_forgotten
    @expected.subject = 'PasswordMailer#forgotten'
    @expected.body    = read_fixture('forgotten')
    @expected.date    = Time.now

    assert_equal @expected.encoded, PasswordMailer.create_forgotten(@expected.date).encoded
  end

  private
    def read_fixture(action)
      IO.readlines("#{FIXTURES_PATH}/password_mailer/#{action}")
    end

    def encode(subject)
      quoted_printable(subject, CHARSET)
    end
end
