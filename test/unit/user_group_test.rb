require File.dirname(__FILE__) + '/../test_helper'

class UserGroupTest < Test::Unit::TestCase
  fixtures :user_groups

  def setup
    @user_group = UserGroup.find(1)
  end

  # Replace this with your real tests.
  def test_truth
    assert_kind_of UserGroup,  @user_group
  end
end
