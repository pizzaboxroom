require File.dirname(__FILE__) + '/../test_helper'

class MyfileTest < Test::Unit::TestCase
  fixtures :myfiles

  def setup
    @myfile = Myfile.find(1)
  end

  # Replace this with your real tests.
  def test_truth
    assert_kind_of Myfile,  @myfile
  end
end
